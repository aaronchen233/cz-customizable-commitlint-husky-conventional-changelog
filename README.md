# 前言

---

> 1. 创建 git 仓库 `git init`
> 2. git 忽略规则 `.gitignore` : `node_modules`
> 3. [代码](https://gitee.com/aaronchen233/cz-customizable-commitlint-husky-conventional-changelog)

# 1. Gitmoji Commit Message 规范

---

- 格式：

```
<emoji> <type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

- 例子：

```
✨ feat(blog): add comment section
```

- [gitmoji | An emoji guide for your commit messages](https://gitmoji.dev/)

# 2. 书写 Commit

---

- cz-customizable
可自定义的 Commitizen 插件（或独立实用程序），可帮助实现一致的提交消息

- 安装

```powershell
npm i -D cz-customizable
```

- 配置
根目录创建 `.cz-config.js`

```js
module.exports = {
  types: [
    {
      value: ':sparkles: feat',
      name: '✨ feat:     新功能'
    },
    {
      value: ':bug: fix',
      name: '🐛 fix:      修复bug'
    },
    {
      value: ':tada: init',
      name: '🎉 init:     初始化'
    },
    {
      value: ':pencil2: docs',
      name: '✏️  docs:     文档变更'
    },
    {
      value: ':lipstick: style',
      name: '💄 style:    代码的样式美化'
    },
    {
      value: ':recycle: refactor',
      name: '♻️  refactor: 重构'
    },
    {
      value: ':zap: perf',
      name: '⚡️ perf:     性能优化'
    },
    {
      value: ':white_check_mark: test',
      name: '✅ test:     测试'
    },
    {
      value: ':rewind: revert',
      name: '⏪️ revert:   回退'
    },
    {
      value: ':package: build',
      name: '📦️ build:    打包'
    },
    {
      value: ':rocket: chore',
      name: '🚀 chore:    构建/工程依赖/工具'
    },
    {
      value: ':construction_worker: ci',
      name: '👷 ci:       CI related changes'
    }
  ],
  messages: {
    type: '请选择提交类型(必填)',
    customScope: '请输入文件修改范围(可选)',
    subject: '请简要描述提交(必填)',
    body: '请输入详细描述(可选)',
    breaking: '列出任何BREAKING CHANGES(可选)',
    footer: '请输入要关闭的issue(可选)',
    confirmCommit: '确定提交此说明吗？'
  },
  allowCustomScopes: true,
  allowBreakingChanges: [':sparkles: feat', ':bug: fix'],
  subjectLimit: 72
}
```

- 将新脚本添加到您的 `package.json`

```
"scripts" : {
  ...
  "commit": "git add . && cz-customizable"
}
```

- 使用 `npm run commit` 代替 `git commit`

# 3. 校验 commit

---

- commitlint

检查 commit 是否符合某种规范的校验工具

- 安装

```powershell
npm i -D @commitlint/cli commitlint-config-git-commit-emoji commitlint-config-cz
```

- 配置
根目录创建 `commitlint.config.js`

```js
module.exports = {
  extends: ['git-commit-emoji', 'cz']
}
```

- husky
针对 git 流程自动执行 commitlint

- 安装

```powershell
npm i -D husky
```

- 配置

```powershell
npm set-script prepare "husky install"
npm run prepare
npx husky add .husky/commit-msg 'npx --no-install commitlint --edit "$1"'
```

# 4. 生成日志

---

- conventional-changelog
可以根据项目的 commit 和 metadata 信息自动生成 changelogs 和 release notes 的系列工具。

- 安装

```powershell
npm i -D conventional-changelog-cli conventional-changelog-gitmoji-config
```

- 将新脚本添加到您的 `package.json`

```
"scripts" : {
  ...
  "changelog": "conventional-changelog -p gitmoji-config -i CHANGELOG.md -s"
}
```

- 运行 `npm run changelog` 生成日志

# 参考文章

---

- [Gitmoji Commit Workflow 完全指南 · 语雀 (yuque.com)](https://www.yuque.com/arvinxx-fe/workflow/gitmoji-commit-workflow)

- [commit规范+commitlint+CHANGELOG自动生成一条龙服务 - 掘金 (juejin.cn)](https://juejin.cn/post/6934292467160514567)
