# 1.0.0 (2022-05-20)


### ✨ Features

* add commitlint 33a1024
* add conventional-changelog 2c3a49a
* add cz-customizable 1521df0
* add husky 9519d75



